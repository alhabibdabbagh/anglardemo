import { Component, OnInit } from '@angular/core';
import { Product } from './product';
import { AlertifyService } from '../services/alertify.service';
import { ProductService } from '../services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Category } from '../catagory/category';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers:[ProductService]

})
export class ProductComponent implements OnInit {

  constructor(private alertService: AlertifyService,
    private productService:ProductService,
    private activatedRoute:ActivatedRoute) { }
  title = "Ürünler listesi"
  filterText = ""
  products: Product[];
  title1 = "ubuntu"
  /*[
    { id: 1, name: "habib", descrptions: "engineering", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 8851517, categoriId: 2 },
    { id: 8, name: "mahmut", descrptions: "doctor", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 99683, categoriId: 1 },
    { id: 2, name: "hasan", descrptions: "law", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 73867, categoriId: 3 },
    { id: 1, name: "habib", descrptions: "engineering", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 83688, categoriId: 1 },
    { id: 8, name: "mahmut", descrptions: "doctor", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 93.84869, categoriId: 1 },
    { id: 1, name: "habib", descrptions: "engineering", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 88866, categoriId: 2 },
    { id: 8, name: "mahmut", descrptions: "doctor", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 9636839, categoriId: 3 },
    { id: 1, name: "habib", descrptions: "engineering", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 88.252, categoriId: 1 },
    { id: 8, name: "mahmut", descrptions: "doctor", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 9.05949, categoriId: 2 },
    { id: 1, name: "habib", descrptions: "engineering", imageUrl: "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80", price: 488, categoriId: 2 }
  ]*/
  ngOnInit(): void{
    this.activatedRoute.params.subscribe(params=> {
      this.productService.getProducts(params["categoryId"]).subscribe(data => {
        this.products=data;
      });
    })
  };
  
  addToCart(product: string): any {
    this.alertService.success(product + " is added");
  }

}
