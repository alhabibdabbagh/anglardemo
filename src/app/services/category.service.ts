import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Category } from '../catagory/category';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { }
  path = "http://localhost:3000/catagory"
  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.path).pipe
      (//get ne tür bir parametre belirtmemiz lazımki çünkü
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }
  handleError(err: HttpErrorResponse) {
    let errorMessage = ''
    if (err.error instanceof ErrorEvent) {
      errorMessage = 'bir hatası oluştu' + err.error.message
    } else {
      errorMessage = 'sistemsel  bir hata'
    }
    return throwError(errorMessage)
  }
}

