import { Component, OnInit } from '@angular/core';
import { Category } from './category'
import { AlertifyService } from '../services/alertify.service';
import { CategoryService } from '../services/category.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-catagory',
  templateUrl: './catagory.component.html',
  styleUrls: ['./catagory.component.css'],
  providers:[CategoryService]
})
export class CatagoryComponent implements OnInit {

  constructor(private alertService: AlertifyService,private categoryService:CategoryService) { }
  title="kategori listesi";
  categorys:Category[];

  ngOnInit(): void {
    this.categoryService.getCategories().subscribe(data => {
      this.categorys=data;
    });
  }
  


}
